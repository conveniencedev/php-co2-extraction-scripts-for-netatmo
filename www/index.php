<?
include "header.php";

title("CO2 Afzuiging");

if (isSet($_GET['drempel_laag']))
{
  file_put_contents ("./config/drempel_temp", $_GET['drempel_temp']);
  file_put_contents ("./config/drempel_uit", $_GET['drempel_uit']);
  file_put_contents ("./config/drempel_laag", $_GET['drempel_laag']);
  file_put_contents ("./config/drempel_medium", $_GET['drempel_medium']);
  file_put_contents ("./config/drempel_hoog", $_GET['drempel_hoog']);
  file_put_contents ("./config/drempel_alarm", $_GET['drempel_alarm']);

  echo "Drempelwaarden aangepast...";
}

if (isSet($_GET['netatmo_appid']))
{
  file_put_contents ("./config/netatmo_appid", $_GET['netatmo_appid']);
  file_put_contents ("./config/netatmo_secret", $_GET['netatmo_secret']);
  file_put_contents ("./config/netatmo_username", $_GET['netatmo_username']);
  file_put_contents ("./config/netatmo_password", $_GET['netatmo_password']);
  file_put_contents ("./config/netatmo_co2_moduleid", $_GET['netatmo_co2_moduleid']);
  file_put_contents ("./config/netatmo_temp_moduleid", $_GET['netatmo_temp_moduleid']);
  file_put_contents ("./config/standaard_snelheid", $_GET['standaard_snelheid']);
  echo "Netatmo configuratie aangepast...";
}


if (isSet($_GET['vorstbeveiliging_reset']))
{
  unlink ("./data/vorstbeveiliging");
  echo "Vorstbeveiliging hersteld...";
}


include "netatmo_config.inc.php";
include "drempel_config.inc.php";

$co2lasterror = file_get_contents("./data/co2lasterror");
$co2lastmeasurement = file_get_contents("./data/co2lastmeasurement");


subtitle("Huidige waarden");
?>
<!-- <span class=ledgroen>-1-</span>
<span class=ledgroen>-1-</span>
<span class=ledgroen>-1-</span>
<span>&nbsp;&nbsp;&nbsp;</span>
<span class=ledgroen>-1-</span> -->
<?
if ($GLOBALS['vorstbeveiliging'])
{
    echo "<b><font color=red>";
    echo "Vorstbeveiliging is actief, ventilator draait niet!";
    echo "</font></b><BR>";
    
    if ($GLOBALS['outsidetemp'] > 0)
    {
      echo "<b>Controleer of ventilator ontdooid is en vrij kan draaien, klik daarna <a href='?vorstbeveiliging_reset'>hier</a> om te resetten.</b>";
    }
    else
    {
      echo "Wacht totdat de temperatuur boven het vriespunt is en reset de vorstbeveiling op deze pagina.";
    }
    echo "<br><br>";

}
?>
<table>
<tr><td>CO2 waarde</td><td><? echo ": <b>".$GLOBALS['co2']." ppm</b>"; ?></td></tr>
<tr><td>Stand ventilator</td><td><? echo ": <b>".$GLOBALS['ventilator']."</b>"; ?></td></tr>
<tr><td>Buitentemperatuur</td><td><? echo ": <b>".$GLOBALS['outsidetemp']." C</b>"; ?></td></tr>
<!-- <tr><td>Luchtdruk</td><td><? echo ": <b>".$GLOBALS['luchtdruk']." mbar</b> (gemeten op moederbord)"; ?></td></tr>
<tr><td>Vochtigheidsgraad</td><td><? echo ": <b>".$GLOBALS['vochtigheidsgraad']." %</b> (gemeten op moederbord)"; ?></td></tr> -->
<tr><td>Temperatuur moederbord</td><td><? echo ": <b>".$GLOBALS['temp']." C</b>"; ?></td></tr>
<tr><td>Laatste meting</td><td>: <b><? echo $co2lastmeasurement; ?></b></td></tr>
<tr><td>Laatste foutmelding</td><td>: <b><? echo $co2lasterror; ?></b></td></tr>
</table>
<br><a href=''>Ververs</a>

<?
subtitle("Netatmo configuratie");
?>
<p>Met een Netatmo (binnen-)module kan de CO2 waarde gemeten worden. Let op: een internet verbinding is vereist.</p>
<form>
<table>
<tr><td>App id</td><td>:<input type='text' name='netatmo_appid' value='<? echo $GLOBALS['netatmo_appid']; ?>' size=32></td></tr>
<tr><td>App secret</td><td>:<input type='text' name='netatmo_secret' value='<? echo $GLOBALS['netatmo_secret']; ?>' size=32></td></tr>
<tr><td>E-mail adres</td><td>:<input type='text' name='netatmo_username' value='<? echo $GLOBALS['netatmo_username']; ?>' size=32></td></tr>
<tr><td>Password</td><td>:<input type='password' name='netatmo_password' value='<? echo $GLOBALS['netatmo_password']; ?>' size=32></td></tr>
<tr><td>ID van binnen module</td><td>:<input type='text' name='netatmo_co2_moduleid' value='<? echo $GLOBALS['netatmo_co2_moduleid']; ?>' size=32><br><i>(wordt gebruikt om CO2 te meten, bv. 70:ee:50:02:55:b2)</i></td></tr>
<tr><td>ID van buiten module</td><td>:<input type='text' name='netatmo_temp_moduleid' value='<? echo $GLOBALS['netatmo_temp_moduleid']; ?>' size=32><br><i>(wordt gebruikt om de ventilator uit te zetten bij een bepaalde buitentemperatuur, bv. 02:00:00:02:27:c4)</i></td></tr>
<tr><td>Standaard snelheid</td><td>

<?
  if ($GLOBALS['standaard_snelheid'] == "uit")
  {
    $selected1="selected";
  }
  else if ($GLOBALS['standaard_snelheid'] == "laag")
  {
    $selected2="selected";
  }
  else if ($GLOBALS['standaard_snelheid'] == "medium")
  {
    $selected3="selected";
  }
  else if ($GLOBALS['standaard_snelheid'] == "hoog")
  {
    $selected4="selected";
  }
?>

<select name=standaard_snelheid>
<option value='uit' <? echo $selected1; ?>>uit</option>
<option value='laag' <? echo $selected2; ?>>laag</option>
<option value='medium' <? echo $selected3; ?>>medium</option>
<option value='hoog' <? echo $selected4; ?>>hoog</option>
<select>
<br><i>(wanneer de netatmo module niet gevonden kan worden, wordt deze snelheid gebruikt)</i>
<tr><td></td><td><input type=submit value=update></td></tr>
<tr><td></td><td><i>Bij een nieuw account moet een nieuw appid gecreeerd worden, dat kan <a href='http://dev.netatmo.com/dev/createapp'>hier</a></i></td></tr>
</table>



</form>
<?

subtitle("Drempelwaarden");

?>
<form>
<table>
<tr><td>Ventilator uit:</td><td> =&lt;<input type='text' name='drempel_uit' value='<? echo $GLOBALS['drempel_uit']; ?>' size=5>ppm</td></tr>
<tr><td>Ventilator laag:</td><td>&gt;<input type='text' name='drempel_laag' value='<? echo $GLOBALS['drempel_laag']; ?>' size=5>ppm</td></tr>
<tr><td>Ventilator medium:</td><td>&gt;<input type='text' name='drempel_medium' value='<? echo $GLOBALS['drempel_medium']; ?>' size=5>ppm</td></tr>
<tr><td>Ventilator hoog:</td><td>&gt;<input type='text' name='drempel_hoog' value='<? echo $GLOBALS['drempel_hoog']; ?>' size=5>ppm</td></tr>
<tr><td>Alarm piep:</td><td>&gt;<input type='text' name='drempel_alarm' value='<? echo $GLOBALS['drempel_alarm']; ?>' size=5>ppm</td></tr>
<tr><td><br></td></tr>
<tr><td><b>Vorstbeveiliging<b></td></tr>
<tr><td>Ventilator uit bij temperatuur:</td><td>&lt;<input type='text' name='drempel_temp' value='<? echo $GLOBALS['drempel_temp']; ?>' size=5>C</td></tr>
<tr><td><i>De ventilator moet op deze pagina gereset worden wanneer deze door de vorstbeveiliging is uitgeschakeld.</i></td></tr>
<tr><td></td><td><input type=submit value=update></td></tr>
</table>
</form>

<? subtitle("Documentatie"); ?>
<a href="/documentatie/">Klik hier</a>

<?
include "footer.php";
?>