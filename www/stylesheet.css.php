<?php

// Header is needed for stylesheets with .php extension (in HTML5 at least)!
header('Content-type: text/css');

function css_radius_all($radius)
{
	echo "-moz-border-radius: ${radius}px;".     // Firefox
	"-webkit-border-radius: ${radius}px;".  // Safari
	"border-radius: ${radius}px;";          // CSS3 (IE 9
}

function css_radius_bottomleft($radius)
{
echo "-moz-border-radius-bottomleft: ${radius}px;".
"-webkit-border-bottom-left-radius: ${radius}px;".
"border-bottom-left-radius: ${radius}px;";
}

function css_radius_bottomright($radius)
{
echo "-moz-border-radius-bottomright: ${radius}px;".
	"-webkit-border-bottom-right-radius: ${radius}px;".
	"border-bottom-right-radius: ${radius}px;";
}

function css_radius_topleft($radius)
{
echo "-moz-border-radius-topleft: ${radius}px;".
"-webkit-border-top-left-radius: ${radius}px;".
"border-top-left-radius: ${radius}px;";
}

function css_radius_topright($radius)
{
echo "-moz-border-radius-topright: ${radius}px;".
"-webkit-border-top-right-radius: ${radius}px;".
	"border-top-right-radius: ${radius}px;";
}

function steunkleur() {
	if (isSet($_REQUEST['color'])) {
		echo $_REQUEST['color'];
	} else {
		echo "000000";
	}
}

// Let op: niet compatible, ff en ie draaien een kwart slag
function gradient($altcolor, $color1, $color2) {
	// http://www.colorzilla.com/gradient-editor/
	echo "background: $altcolor;";
	echo "background: -moz-linear-gradient(top, $color1 0%, $color2 100%);"; // FF3.6+ 
	echo "background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,$color1), color-stop(100%,$color2));"; // Chrome,Safari4+
	echo "background: -webkit-linear-gradient(top, $color1 0%,$color2 100%);"; // Chrome10+,Safari5.1+
	echo "background: -o-linear-gradient(top, $color1 0%,$color2 100%);"; // Opera 11.10+
	echo "background: -ms-linear-gradient(top, $color1 0%,$color2 100%);"; // IE10+
	echo "background: linear-gradient(to bottom, $color1 0%,$color2 100%);"; // W3C
	echo "filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$color1', endColorstr='$color2',GradientType=0 );"; // IE6
}


?>

@font-face {
  font-family: myHelveticaLight;
  src: local("Helvetica Neue Light"),
       local("HelveticaNeue-Light"),
       url(./HelveticaNeueLight.ttf);
}

body {
	background: white;
	margin: 0px;
	padding: 20px;
	font-family: myHelveticaLight, "Arial";
	font-size: 14px;
}

table tr td {
	font-family: myHelveticaLight, "Arial";
	font-size: 14px;
}

a {
	color: <? steunkleur(); ?>;
	text-decoration: none;
	font-weight: bold;
}

img {
	border: 0px;
}

th {
	background: #065999;
	color: white;
	font-family: myHelveticaLight, "Arial";
	padding-left: 30px;
	padding-right: 30px;
	  <? css_radius_all(10); ?>
}

h1,h2,h3 {
   font-family: myHelveticaLight;
   font-weight: normal;
}

h1 {
   color: <? steunkleur(); ?>;
}


span.data
{
	background: #FFFAE9;
	padding-left: 10px;
	padding-right: 10px;
	font-weight: bold;
	border: 1px dashed black;
}

form {
  display: inline-block;
  border: 1px dashed black;
  padding: 20px;
  margin: 0px;
  color: #000000;
  background: rgba(128,128,128,0.2);
  <? css_radius_all(10); ?>
}

.ledgroen
{
  <? css_radius_all(50); ?>
  background: green;
  height: 32px;
  width: 32px;
}

hr
{
  border-top: 1px dashed black;
}