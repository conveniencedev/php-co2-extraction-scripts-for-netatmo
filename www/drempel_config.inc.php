<?
# Sorry for the Dutch language
# drempel = threshold
# uit = off, aan = on, laag = low, medium = medium, hoog = high
# vorstbeveiliging = frost protection
#
  $GLOBALS['drempel_temp'] = file_get_contents ("./config/drempel_temp");
  $GLOBALS['drempel_uit'] = file_get_contents ("./config/drempel_uit");  
  $GLOBALS['drempel_laag'] = file_get_contents ("./config/drempel_laag");
  $GLOBALS['drempel_medium'] = file_get_contents ("./config/drempel_medium"); 
  $GLOBALS['drempel_hoog'] = file_get_contents ("./config/drempel_hoog"); 
  $GLOBALS['drempel_alarm'] = file_get_contents ("./config/drempel_alarm"); 
  $GLOBALS['vorstbeveiliging'] = file_exists("./data/vorstbeveiliging");   
?>