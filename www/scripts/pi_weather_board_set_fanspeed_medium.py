#!/usr/bin/python

# PiWeather Board
# Read and displays the sensors values

import smbus

bus = smbus.SMBus(1) # for Raspberry Pi version 2

address = 0x4E # i2c board address

# All off
# 0x25 = high (on)
# 0x26 = low (off)

bus.write_byte_data(address,0x26, 0)
bus.write_byte_data(address,0x26, 1)
bus.write_byte_data(address,0x26, 3)
bus.write_byte_data(address,0x26, 4)

bus.write_byte_data(address,0x25, 2)


