#!/usr/bin/python

# PiWeather Board
# Read and displays the sensors values

import smbus

bus = smbus.SMBus(1) # for Raspberry Pi version 2

address = 0x4E # i2c board address

# Read board version
ver = bus.read_byte_data(address,0x24)
print "Board_version =",ver

# Temperature
temp_inside = bus.read_byte_data(address,0x42)
print "Board_temp =", temp_inside, "C"

# Humidity
humidity = bus.read_byte_data(address,0x43)
print "Humidity =", humidity, "%"

# Pressure
pressure = bus.read_byte_data(address,0x47)
print "Pressure =", pressure, "mbar"


# External temp sensors
#temp_outside_1 = bus.read_word_data(address,0x45)
#temp_outside_1 = 0.1*temp_outside_1

#temp_outside_2 = bus.read_word_data(address,0x46)
#temp_outside_2 = 0.1*temp_outside_2


